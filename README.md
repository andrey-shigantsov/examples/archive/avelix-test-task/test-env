Тестирование Service-TestTaskCpp-Avelix
================================================================================

## Подготовка среды запуска

**Установите клиент MySQL**

В linux используется MariaDB client, в windows+cygwin MySQL client будет установлен

**Сконфигурируйте service.testtask.avelix**

Поместите файл etc/service.ini в корень с service.testtask.avelix
  
Также при запуске можно определиь/переопределить все необходимые параметы
через аргументы приложения 

## Тестирование

**Запустите service.testtask.avelix в терминале и дождитесь запуска сервиса**
   
В случае успешного запуска будет выведено сообщение "listen port {port}..."

В случае проблем будут выводится соответствующие сообщения
   
**Передайте пакеты сервису**

В linux и windows+cygwin можно воспользоваться netcat

```bash
cd {RepRoot}/packet
cat {pack_name} | nc -c {host} {port}
```
		
**Убедитесь, что сервис работает верно**

Откройте таблицу [PersonalData](https://www.db4free.net/phpMyAdmin/index.php?db=testdb_avelix&table=PersonalData&target=sql.php)
	
	host: db4free.net
	user: andrey_sh
	password: time1700
	db: testdb_avelix
	
Сверте данные переданных пакетов с записями БД и выводом в service.log
	
	


